from sqlalchemy import *
from migrate import *
from app import db

from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()

service_history = Table('service_history', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('value', INTEGER),
    Column('code', VARCHAR(length=255)),
    Column('type', VARCHAR(length=120)),
    Column('last_check', DATETIME),
)
from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO
engine = create_engine(SQLALCHEMY_DATABASE_URI, echo = True)
engine.create_all(pre_meta)

def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    print('asdfsad')

    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['service_history'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine

