import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-dashboard-react/components/tableStyle.js";
const useStyles = makeStyles(styles);

export default function CustomTableWrapper(Component) {
  return function Table(props) {
    const classes = useStyles();
    return (
        <Component {...props} classes={classes} />
    );
  }
}