from app import db
import datetime

ROLE_USER = 0
ROLE_ADMIN = 1

class Stream(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(64), index = True, unique = True)
    url = db.Column(db.String(120), unique = True)
    service_stream_name = db.Column(db.String(120), index = True, unique = True)
    date_start = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Stream %r>' % (self.title)

class ServiceHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(64), index = True, unique = True)
    type = db.Column(db.String(255))
    file_path = db.Column(db.String(500))
    last_check = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    value = db.Column(db.Integer)