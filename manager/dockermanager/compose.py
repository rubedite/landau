import yaml, subprocess

class Compose:
    def __init__(self, file_path):
        file = open(file_path, 'r')
        yml = file.read()
        file.close()

        self.path = file_path
        self.yml = yaml.load(yml)

    def get_services(self):
        return self.yml['services']

    def clear(self):
        new = {}
        for key in self.yml['services']:
            if not key.startswith('stream_reader'):
                new[key] = self.yml['services'][key]

        self.yml['services'] = new

    def add_service(self, name, service):
        self.yml['services'][name] = service

    def remove_service(self, name):
        del self.yml['services'][name]

    def save(self):
        file = open(self.path, 'w')
        file.write(yaml.dump(self.yml))
        file.close()


    def run_service(self, process):
        subprocess.call(
            ('docker-compose -f ' + self.path + ' up -d ' + process),
            shell=True)

    def stop_service(self, process):
        subprocess.call(
            ('docker-compose -f ' + self.path + ' rm -f -s -v ' + process),
            shell=True)

    def up(self):
        subprocess.call(
            ('docker-compose -f ' + self.path + ' up -d --build'),
            shell=True)

    def down(self):
        subprocess.call(
            ('docker-compose -f ' + self.path + ' down'),
            shell=True)