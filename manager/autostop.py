import os, datetime, time, psycopg2
from dockermanager.compose import Compose
from app import models

DIRECTORY_TO_WATCH = "../files"
compose_file_path = '../docker/docker-compose.yml'

if __name__ == '__main__':
    arFiles = {}

    while True:
        for root, subdirs, files in os.walk(DIRECTORY_TO_WATCH):
            if root.find('/done') > -1:
                continue

            path = root
            path = path.replace(DIRECTORY_TO_WATCH, '')
            path = path.replace('/', '')

            if path:
                if not path in arFiles:
                    arFiles[path] = 0

                if arFiles[path] > 0 and (len(files) - 10) > arFiles[path]:
                    stream = models.Stream.query.filter_by(title=path).first()
                    if stream:
                        yaml = Compose(compose_file_path)
                        yaml.stop_service(stream.service_stream_name)
                        yaml.remove_service(stream.service_stream_name)
                        yaml.save()

                arFiles[path] = len(files)

        time.sleep(2)
