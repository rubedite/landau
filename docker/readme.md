# Install and run

```
apt-get install openssl curl docker docker-compose

cd recognition/models/

chmod +x download.sh && ./download.sh

cd ../../docker

cp .env.example .env

sudo sysctl -w vm.max_map_count=262144

docker-compose up -d

```

# Resources

## RabbitMQ Admin panel

[http://localhost:8888](http://localhost:8888)

## Elastic Admin panel

[http://localhost:5000](http://localhost:5000)

## Search web client

[http://localhost:1358](http://localhost:1358)