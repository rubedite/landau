youtube-dl -4 -f best --prefer-ffmpeg --no-color --no-cache-dir --no-progress -o - $2 \
| ffmpeg -i - -vn -ab 32k -acodec pcm_s32le -ac 1 -ar 32000 -f sox - | \
sox -t wav -p -r 16000 -c 1 -b 16 "/files/$1/$3_.wav" silence -l 1 0.2 0.3% 1 0.7 $3% : newfile : restart