# -*- coding: utf-8 -*-
import pika, json
import asyncio
import websockets
import functools
from threading import Thread

credentials = pika.PlainCredentials('дфт', 'guest')
parameters = pika.ConnectionParameters('rabbit', 5672, '/', credentials)
connected = set()


class ConsumerThread(Thread):
    _loop = None

    def __init__(self, *args, **kwargs):
        self._loop = asyncio.new_event_loop()
        super(ConsumerThread, self).__init__(*args, **kwargs)

    async def send(self, text):
        import datetime
        text = str(datetime.datetime.now()) + ": %r" % text
        if connected:
            await asyncio.wait([ws.send(text) for ws in connected])

    # Not necessarily a method.

    def callback(self, channel, method, properties, body):
        message = json.loads(body)
        self._loop.run_until_complete(self.send(message['text']))

    def run(self):
        try:
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue='output')

            channel.basic_consume(queue='output', auto_ack=True, on_message_callback=self.callback)

            channel.start_consuming()
        except pika.exceptions.AMQPConnectionError:
            print("output: Waiting for rabbit server")
            exit()

if __name__ == "__main__":
    try:
        connection = pika.BlockingConnection(parameters)
        connection.close()
        thread = ConsumerThread()
        thread.start()
        print('connection started')

    except pika.exceptions.AMQPConnectionError:
        print("output: Waiting for rabbit server")
        exit()

    async def hello(websocket, path):
        connected.add(websocket)
        print(connected)
        name = await websocket.recv()
        print(f"< {name}")

        greeting = f"Hello {name}!"

        await websocket.send(greeting)
        print(f"> {greeting}")


    start_server = websockets.serve(hello, "python_output", 80)
    print('socket server started')
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()




