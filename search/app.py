# -*- coding: utf-8 -*-

from flask import Flask, request, send_from_directory, abort
from elasticsearch import Elasticsearch
import json

app = Flask(__name__, static_url_path='',
            static_folder='static')

es = Elasticsearch(['landau_elastic', 'landau_elastic2'])

@app.route("/")
def root():
    return app.send_static_file('index.html')

@app.route("/search")
def search():
    q = request.args.get("q")

    res = es.search(index="recognized", body={"query": {"fuzzy": {"text": q}}})
    hits = []
    for hit in res['hits']['hits']:
        hits.append(hit['_source'])

    return json.dumps(hits)

@app.route("/hints")
def hints():
    hints = [
        {'text': u'ввод'},
        {'text': u'вывод'},
        {'text': u'средств'},
        {'text': u'путин'},
        {'text': u'наркотики'},
        {'text': u'грибы'},
        {'text': u'бомбы'},
    ]

    q = request.args.get("q")
    hints_result = []

    for hint in hints:
        if q.lower() in hint['text']:
            hints_result.append(hint)

    return json.dumps(hints_result)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
