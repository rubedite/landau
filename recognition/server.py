# -*- coding: utf-8 -*-

import pika, sys, json, os
from deepspeech.model import Model
# from deepspeech import Model
import scipy.io.wavfile as wav
import subprocess
import re, shutil
import psycopg2
import numpy as np
from difflib import SequenceMatcher
import speech_recognition as sr
import urllib
from external import Yandex
from elasticsearch import Elasticsearch

# es = Elasticsearch(['landau_elastic', 'landau_elastic2'])

# for deepspeech 0.2.0a8
ds = Model(sys.argv[1], 26, 9, sys.argv[2], 900)
ds.enableDecoderWithLM(sys.argv[2], sys.argv[3], sys.argv[4], 1.85, 2.00, 1.50)



ds2 = Model(sys.argv[1], 26, 9, sys.argv[2], 500)
ds2.enableDecoderWithLM('./models/model_18_wer/alphabet.txt', './models/model_18_wer/lm.binary', './models/model_18_wer/trie', 1.75, 1.00, 1.00)

r = sr.Recognizer()
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('rabbit',
                                       5672,
                                       '/',
                                       credentials)


def convert_samplerate(audio_path):
    sox_cmd = 'sox {} --type raw --bits 16 --channels 1 --rate 16000 - '.format(audio_path)
    try:
        p = subprocess.Popen(sox_cmd.split(),
                             stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, err = p.communicate()

        if p.returncode:
            raise RuntimeError('SoX returned non-zero status: {}'.format(err))

    except OSError as e:
        raise OSError('SoX not found, use 16kHz files or install it: ', e)

    audio = np.fromstring(output, dtype=np.int16)

    return 16000, audio


def recognition(file):
    text = ''
    text2 = ''
    text_3 = ''

    if os.path.isfile(file):
        size = os.path.getsize(file)
        if size < 1000000:
            try:
                fs, audio = wav.read(file)

                if fs != 16000:
                    if fs < 16000:
                        print(
                                ('Warning: original sample rate (%d) is lower than 16kHz.' +
                                 'Up-sampling might produce erratic speech recognition.') % (fs)
                        )
                    fs, audio = convert_samplerate(file)

                text = ds.stt(audio, fs)
                text2 = ds2.stt(audio, fs)

                try:
                    text_3 = Yandex.recognize(file)
                except urllib.error.HTTPError:
                    print('Yandex error')


                # with sr.AudioFile(file) as source:
                #     try:
                #         r.adjust_for_ambient_noise(source)
                #         audio = r.record(source)
                #
                #         text2 = r.recognize_google(audio, language="ru-RU")
                #         print('Google', text2)
                #     except sr.UnknownValueError:
                #         print('Can not parse data for google')
                #     except OSError:
                #         print('Unable allocate memory')


            except ValueError:
                print('Unexpected error in file')


    result = {
        'text_local': text,
        'text_google': text2,
        'text_yandex': text_3,
        'ratio': SequenceMatcher(None, text, text_3).ratio() if text_3 else None,
    }

    return result


try:
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='for_recognition')
    channel.queue_declare(queue='output')
    conn = psycopg2.connect(dbname='landau', user='postgres',
                            password='password', host='db')
    print("Connected")

    def callback(ch, method, properties, body):
        import datetime
        message = json.loads(body)
        print(message)
        if message['file']:
            # fs, audio = wav.read(message['file'])
            # processed_data = ds.stt(audio, fs)
            if not os.path.isfile(message['file']):
                print('file error')
                ch.basic_ack(delivery_tag=method.delivery_tag)
                return

            result = recognition(message['file'])

            ar_file = message['file'].split('/')
            file_name = ar_file.pop()

            file_name = file_name.replace('.wav',
                                          '_' + str(datetime.datetime.now().timestamp()) + '.wav')

            ar_file.append(file_name)

            ar_file[1] = 'archive'

            body = {
                'text': result['text_local'],
                'text_google': result['text_google'],
                'text_yandex': result['text_yandex'],
                'ratio': result['ratio'],
                'file': '/'.join(ar_file),
                'source': message['source'],
                'date': message['date'],
            }

            # es.index(index="recognized", body=body)

            shutil.move(message['file'], '/'.join(ar_file))

            cursor = conn.cursor()

            cursor.execute('insert into service_history (value, code, type, last_check) values (%s, %s, %s, %s)',
                (
                    result['ratio'],
                    message['source'],
                    'recognition',
                    datetime.datetime.now()
                )
            )

            if result['text_local']:
                cursor.execute('insert into recognized_text (code, text, date_start, file_path) values (%s, %s, %s, %s)',
                    (
                        message['source'],
                        'New: '+ result['text_local'] + '<br />Old: ' + result['text_google'] + '<br />Yandex: ' + result['text_yandex'],
                        datetime.datetime.strptime(message['date'], "%Y-%m-%d %H:%M:%S"),
                        '/'.join(ar_file)
                    )
                )

                channel.basic_publish(exchange='',
                                      routing_key='output',
                                      body=json.dumps({
                                          'source': message['source'],
                                          'text': result['text_local']
                                      }))

            conn.commit()
            cursor.close()

            print(body)

            ch.basic_ack(delivery_tag=method.delivery_tag)


    channel.basic_qos(prefetch_count=1)

    channel.basic_consume(queue='for_recognition',
                          on_message_callback=callback)

    channel.start_consuming()

except pika.exceptions.AMQPConnectionError:
    print("recognition: Waiting for rabbit server")
