#!/bin/sh

wget --no-check-certificate --no-proxy 'http://landau-rubedite.s3.eu-central-1.amazonaws.com/trie'
wget --no-check-certificate --no-proxy 'http://landau-rubedite.s3.eu-central-1.amazonaws.com/alphabet.txt'
wget --no-check-certificate --no-proxy 'http://landau-rubedite.s3.eu-central-1.amazonaws.com/lm.binary'
wget --no-check-certificate --no-proxy 'http://landau-rubedite.s3.eu-central-1.amazonaws.com/output_graph.pb'