import soundfile as sf
import os

class Yandex:
    @staticmethod
    def recognize(file_path):
        import urllib.request
        import json

        FOLDER_ID = "b1gsc4ponvr992g9hjrf"  # Идентификатор каталога
        IAM_TOKEN = "CggVAgAAABoBMxKABDg8Cvaa5tvjtnAay1RJjx0-N5HoW7KAee34Q7kihvEfcFy0AJBpoVVKsGHUK1l5kMRKPOG4qFEIIa8hAFy-kjb7gRHWJS6SCaLdZMTJIrv8d54LvQlmjKuPkvMNdBXFGxR0791dvIH6-Avqp1AnR7ln_Wyjx22rGIs9Q76HfGN6yTzl_XTNF7aps1qDpgOpjNvY0bzfE6nt6Houo3rKN9GEFj-3BPur4cSL4UUtQcduPfMpwtfPV41KZ3dLLHyaS2e015torGNmmCIGaVcbaZf0B45CrKhohpoqJj9l2-H5KaPO8-CfEu0XkxCXZhhLEFIRCaxDR3PypAA71tLIst91ZlK3g-wZuGvh6Zv9GcZdnkjPSRSdKjvDw-orIqoLvV319zVkBoCrRugZIQ0-Zl2CelMDzJXsV3Q82WdR4DSUjV1Hmg_O52UNnUQ4J5qN3Tyy89Ra9Mw3SCaCL8Indqjtr-xrYlXL6d31oFHIQc6w6l6lNxwMTMIS3c0ysGmfBpXq50Q6ySEKNZ7WJafi-vVtwBhEnSoIKED2njXzNvcE1zzLlP4gY6QEl15ZL82ANK0fJiNj0juIBAWGwIUvasS87__bNGa1EWXVClWb7DoWIDwIYBNHbtGo7Hk4Y1YZgOF3wkDZYA7qDZHqwV_zN_bZLxhfJqfo9I5fC93FIRKBGiQQhb-47wUYxZC77wUiFgoUYWplaDJvN21jMjA1aW44dHIxMm4="  # IAM-токен
        data, samplerate = sf.read(file_path)
        sf.write(file_path+'.ogg', data, samplerate)
        with open(file_path+'.ogg', "rb") as f:
            data = f.read()

        params = "&".join([
            "topic=general",
            "folderId=%s" % FOLDER_ID,
            "lang=ru-RU"
        ])

        url = urllib.request.Request("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?%s" % params, data=data)
        url.add_header("Authorization", "Bearer %s" % IAM_TOKEN)
        f.close()
        responseData = urllib.request.urlopen(url).read().decode('UTF-8')
        decodedData = json.loads(responseData)
        os.remove(file_path+'.ogg')

        if decodedData.get("error_code") is None:
            return decodedData.get("result")
