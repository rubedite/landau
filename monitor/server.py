import os, datetime, time, psycopg2

DIRECTORY_TO_WATCH = "/files"

if __name__ == '__main__':
    conn = psycopg2.connect(dbname='landau', user='postgres',
                            password='password', host='db')

    while True:
        for root, subdirs, files in os.walk(DIRECTORY_TO_WATCH):
            if root.find('/done') > -1:
                continue

            path = root
            path = path.replace(DIRECTORY_TO_WATCH, '')
            path = path.replace('/', '')

            if path:
                total_size = 0

                for f in files:
                    fp = os.path.join(root, f)

                    if not os.path.islink(fp) and os.path.isfile(fp):
                        total_size += os.path.getsize(fp)

                cursor = conn.cursor()
                cursor.execute(
                    'select value from service_history where code=%s and type=\'stream\' order by last_check desc limit 1',
                    (path,)
                )
                res = cursor.fetchone()
                print(res)
                print(path)
                print(total_size)
                if not res or not res[0] or total_size != int(res[0]):
                    cursor.execute('insert into service_history (value, code, type, last_check) values (%s, %s, %s, %s)',
                                (total_size, path, 'stream', datetime.datetime.now())
                                )
                    conn.commit()

                cursor.close()

        time.sleep(2)
